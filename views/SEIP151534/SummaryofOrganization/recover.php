<?php
require_once ("../../../vendor/autoload.php");
use App\SummaryofOrganization\SummaryofOrganization;

$objSummaryofOrganization = new SummaryofOrganization();
$objSummaryofOrganization->setData($_GET);
$objSummaryofOrganization->recover();
