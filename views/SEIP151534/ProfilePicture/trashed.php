
<?PHP
require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;
use  App\Message\Message;
use App\Utility\Utility;

$pic=new ProfilePicture();

$allData=$pic->trashed("obj");



######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $pic->trashedPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################


?>
<!Doctype html>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
<link rel="stylesheet" href="../../../resource/assets/css/style.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="../../../resource/assets/bootstrap/css/s.css">





<h2>Trashed List</h2>

<div align="center">
    <div id="TopMenuBar">
        <button type="button" onclick="window.location.href='../index.php'" class=" btn-info btn-lg">Home</button>
        <button type="button" onclick="window.location.href='create.php'" class=" btn-info btn-lg">Add new</button>

    </div>
</div>
<br>

<?php

$serial =1;

echo "<table class=\"table table-bordered table-hover my-table-border my-td\">";

echo "<tr><th>Serial</th><th>ID</th><th>Name</th><th>Picture</th><th>Action</th></tr>";

foreach($someData as $oneData){

    echo "<tr>";
    echo "<td>$serial</td>";
    echo "<td>$oneData->id</td>";
    echo "<td>$oneData->name</td>";
    echo "<td><img src='$oneData->profile_image' alt='' width='200'></td>";
    echo "<td>


    <a href='recover.php?id=$oneData->id'><button class='btn btn-success'>Recover</button></a>
<a href='delete.php?id=$oneData->id'> <button class=\"btn btn-warning btn-sm\">Delete</button>  </a></td>";
    echo "</tr>";
    $serial++;
}



?>




<html>
<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="center" class="container">
    <ul class="pagination">
        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;
        if($page>$pages) Utility::redirect("trash.php?Page=$pages");

        if($page>1)  echo "<li><a href='trashed.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='trashed.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value; " >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->

</html>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->


