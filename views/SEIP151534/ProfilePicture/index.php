<?php

require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;


$pic = new ProfilePicture;
$allData =  $pic->index('obj');


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Atomic Project</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/s.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container-fluid wrapper">


    </div>



            <div class="col-md-10 ">
                <div class="atomic-nav">
                    <div class="navbar">
                        <div class="container">
                            <div class="navbar-header">

                            </div>
                            <div align="center">
                                <div id="TopMenuBar">
                                    <button type="button" onclick="window.location.href='../index.php'" class=" btn-info btn-lg">Home</button>
                                    <button type="button" onclick="window.location.href='create.php'" class=" btn-info btn-lg">Add new</button>
                                    <button type="button" onclick="window.location.href='trash_item.php'" class=" btn-info btn-lg">Trashed List</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 ">

                </div>



            </div>
        </div>
    </div>
    <hr class="hr-divider">
    <div class="footer">
        <div class="row">

            <div class="user-img col-md-6">
            </div>
        </div>
    </div>
</div>




<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<script>
    $("#alertmsg").fadeTo(2000, 500).slideUp(500, function(){
        $("#alertmsg").slideUp(500);
    });

</script>


</body>

</html>

