<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Hobbies\Hobbies;
use App\Utility\Utility;
use App\Message\Message;





?>

<!DOCTYPE html>

<head>
    <title>ATOMIC PROJECTS - INDEX PAGE</title>
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../resource/assets/css/style.css">
    <link rel="stylesheet" href="../../resource/assets/bootstrap/css/s.css">


</head>
<body>
<div class="container">
    <div class="panel-heading h1"> Atomic Project</div>


    <table align="center">



                <div id="Sidebar" >

                    <button type="button" onclick="window.location.href='BookTitle/index.php'" class=" btn-info btn-lg">Book Title</button>
                    <button type="button" onclick="window.location.href='Birthday/index.php'" class=" btn-info btn-lg">Birthday</button>

                    <button type="button" onclick="window.location.href='City/index.php'" class=" btn-info btn-lg">City</button>
                    <button type="button" onclick="window.location.href='Email/index.php'" class=" btn-info btn-lg">Email Subscription</button>
                    <button type="button" onclick="window.location.href='Gender/index.php'" class=" btn-info btn-lg">Gender</button>
                    <button type="button" onclick="window.location.href='Hobbies/index.php'" class=" btn-info btn-lg">Hobby</button>
                    <button type="button" onclick="window.location.href='ProfilePicture/index.php'" class=" btn-info btn-lg">Profile Picture</button>
                    <button type="button" onclick="window.location.href='SummaryOfOrganization/index.php'" class=" btn-info btn-lg">Summary Of Organization</button>




                </div>

    </table>
</br></br></br>
<div align="center">
    <div class="panel panel-success col-md-18 margintop100">
        <div class="panel-heading h3">Welcome to Atomic Project</div>
        <div class="panel-body">This Atomic project contains the details of eight categories ,such as book title, birthday, city, email, gender, hobbies, profile and summary.Here we used Object Oriented php, html, css, javascript, jquary, PDO database connection, modal, tooltip etc. Hope, this project will help new learners to understand the basic things in web development sector.</div>
    </div>

</div>
</div>
</body>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>




<script>
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete ID# "+id+" ?");
        if (x)
            return true;
        else
            return false;
    }




    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

    $(document).ready(function() {
        $("#checkall").click(function() {
            var checkBoxes = $("input[name=mark\\[\\]]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
    });





</script>


</HTML>

